import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodapp/pages/queue.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'api-requests/requests.dart';
import 'globels.dart';
import 'models/Login.dart';
import 'pages/loginPage.dart';
import 'pages/stackpage.dart';
import 'route_generator.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: mainColor, // navigation bar color
    statusBarColor: mainColor, // status bar colorr
  ));

  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var flag = prefs.getBool('isLogin');

  String route;
  if (flag == null) {
    route = '/';
  } else {
    route = '/homepage';
    currentLogin = loginFromMap(prefs.getString('user') ?? "");
    globalUID = currentLogin.uid;

    try {
      bool val = await checkToken();
      // print(val);
      if (val == false) {
        route = '/';
      }
    } catch (ex) {
      print(ex);
    }
  }

  startingPage = (route == '/')
      ? LoginPage()
      : StackImplementation();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyApp createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food App',
      theme: ThemeData(
        // backgroundColor: mainColor.shade100,
        primarySwatch: mainColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: startingPage,
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
