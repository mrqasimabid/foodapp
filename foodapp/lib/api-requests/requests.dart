import 'dart:convert';

import 'package:foodapp/globels.dart';
import 'package:foodapp/models/KotDetail.dart';
import 'package:foodapp/models/KotMain.dart';
import 'package:foodapp/models/Tables.dart';
import 'package:foodapp/models/kotSaveRes.dart';
import 'package:foodapp/models/Login.dart';
import 'package:foodapp/models/Items.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> loginUser(String username, String password) async {
  var res;
  bool error = false;
  Login login = new Login();
  try {
    final http.Response response = await http.post(
      Uri.parse(loginURL),
      body: <String, String>{
        'uname': username,
        'pass': password,
      },
    );
    res = response.body;
    login = loginFromMap(res);
  } catch (ex) {
    print(ex);
    error = true;
  }
  if (error) {
    return false;
  } else {
    currentLogin = login;
    globalToken = login.token;
    globalUID = login.uid;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('isLogin');
    prefs.setBool('isLogin', true);
    prefs.setString('user', loginToMap(currentLogin));
    return true;
  }
}

Future<bool> checkToken() async {
  final http.Response response = await http.post(
    Uri.parse(fetchUserURL),
    body: <String, String>{
      'token': currentLogin.token ?? '',
    },
  );
  if (response.body.compareTo("Not A Valid Token") == 0) {
    return false;
  } else {
    return true;
  }
}

Future<List<Items>> getItems() async {
  final http.Response response = await http.post(
    Uri.parse(fetchItemsURL),
    body: <String, String>{
      'token': currentLogin.token ?? '',
    },
  );

  return itemsFromMap(response.body);
}

Future<String> getMaxVrno() async {
  final http.Response response = await http.post(
    Uri.parse(getMaxVrnoURL),
    body: <String, String>{
      'company_id': '1',
      'etype': 'kot_bill',
      'token': currentLogin.token ?? ''
    },
  );
  return response.body;
}

Future<String> fetchKot(vrno) async {
  final http.Response response = await http.post(
    Uri.parse(fetchkotURL),
    body: <String, String>{
      'vrnoa': vrno.toString(),
      'company_id': '1',
      'etype': 'kot_bill'
    },
  );
  return response.body;
}

fetchPrint({vrnoa = '0'}) async {
  final http.Response response = await http.post(
    Uri.parse(
        "${baseUrl}index.php/doc/Print_Voucher/kot_bill/$vrnoa/1/-1/undefined/1/5/lg/?token=${currentLogin.token}"),
    body: <String, String>{
      'company_id': currentLogin.companyId ?? '1',
      'fn_id': currentLogin.fnId ?? '1',
      'etype': 'kot_bill',
    },
  );
  return response.bodyBytes;
}

fetchAllOrders() async {
  final http.Response response = await http.post(
    Uri.parse(fetchAllOrdersURL),
    body: <String, String>{
      'company_id': currentLogin.companyId ?? '1',
      'fn_id': currentLogin.fnId ?? '1',
      'token': currentLogin.token ?? '',
    },
  );
  return response.body;
}

Future<bool?> addKot({
  required KotMain main,
  required List<KotDetail> detail,
  required String vrnoa,
  required String vtype,
}) async {
  main.uid = currentLogin.uid;
  String stockmain = kotMainToMap(main);
  String stockdetail = kotDetailToMap(detail);
  print(stockdetail);
  final http.Response response = await http.post(
    Uri.parse(saveKotURL),
    body: <String, String>{
      'stockmain': stockmain,
      'stockdetail': stockdetail,
      'vrnoa': vrnoa,
      'voucher_type_hidden': vtype,
      'etype': 'kot_bill',
      'token': currentLogin.token.toString(),
    },
  );

  return jsonDecode(response.body)[0] as bool;
}

fetchAllTables() async {
  final http.Response response = await http.post(
    Uri.parse(fetchTablesURL),
    body: <String, String>{
      'token': currentLogin.token ?? '',
    },
  );
  print(response.body);
  return tablesFromMap(response.body);
}
