import 'package:flutter/material.dart';
import 'package:foodapp/models/Items.dart';
import 'package:foodapp/pages/loginPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';
import 'models/Login.dart';
import 'models/TableRow.dart';

String baseUrl2 = "http://resturantdemo.digitalmanager.cloud/";
String baseUrl = "https://barber.digitalmanager.cloud/";
String itemImage = baseUrl + "assets/uploads/items/";

String loginURL = baseUrl + 'index.php/welcome/loginAPI';
String fetchUserURL = baseUrl + 'index.php/webapi/getuserlist';
String fetchTablesURL = baseUrl + 'index.php/webapi/fetchalltables';

//add
String saveKotURL =
    baseUrl + 'index.php/restaurant/holdebil?token=${currentLogin.token}';

//fetch
String fetchItemsURL = baseUrl + 'index.php/webapi/fetchAllItems';
String getMaxVrnoURL =
    baseUrl + 'index.php/restaurant/getMaxVrnoa?token=${currentLogin.token}';
String fetchkotURL =
    baseUrl + 'index.php/restaurant/fetchkot?token=${currentLogin.token}';
String fetchAllOrdersURL = baseUrl +
    'index.php/restaurant/fetch_all_orders?token=${currentLogin.token}';

//globals

Login currentLogin = new Login();
String? globalUID;
String? globalToken;

// String fetchPatientDetailURL = baseUrl + 'index.php/webapi/getPatientDetails';
// String fetchDoctorURL = baseUrl + 'index.php/webapi/fetchAlldoctor';
// String fetchIPDURL = baseUrl + 'index.php/webapi/getipdlist';
// String fetchDiseaseURL = baseUrl + 'index.php/webapi/getdiseaselist';
// String fetchPatientURL = baseUrl + 'index.php/webapi/getpatientlist';
// String addPatientURL = baseUrl + 'index.php/webapi/patientregistration';
// String addUserURL =
//     baseUrl + 'index.php/webapi/saveuser?pkrs=YX_wZGlnaXRfMj.xO.==';
// String fetchUserURL = baseUrl + 'index.php/webapi/getuserlist';
// String saveIDPURL = baseUrl + 'index.php/webapi/ipdregistration';
// String updateDoctor = baseUrl + "index.php/webapi/DoctorProfile";

// String saveCheckin = baseUrl + "index.php/webapi/SaveCheckIn";
// String saveSurgeryDetails = baseUrl + "index.php/webapi/addSurgeryDetails";

// String globalToken = null;
// String addlensDetailURL = baseUrl + "index.php/webapi/savelensDetails";
// String addLabTestDetailURL = baseUrl + "index.php/webapi/addLabTestDetails";
// String getPatientHistoryURL = baseUrl + "index.php/webapi/getPatientHistory";
// String getSurgeryHistoryURL = baseUrl + "index.php/webapi/getSurgeryHistory";
// String getTestListURL = baseUrl + "index.php/webapi/gettestlist";
// String addSugarChartURL = baseUrl + "index.php/webapi/savesugarchart";
// String getSugarChartURL = baseUrl + "index.php/webapi/getsugarchart";

showSnackbar(var key, var msg, String success) {
  Color? clr;
  if (success == "wait") {
    clr = Colors.yellow[700];
  } else if (success == "true") {
    clr = Colors.green;
  } else if (success == "false") {
    clr = Colors.red;
  }
  key.currentState.showSnackBar(new SnackBar(
    content: Text("$msg"),
    duration: Duration(seconds: 2),
    backgroundColor: clr,
  ));
}

logout(context) async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  pref.remove('user');
  pref.remove('isLogin');
  pref.remove('doctor');

  Navigator.pushAndRemoveUntil(
    context,
    MaterialPageRoute(
      builder: (BuildContext context) => LoginPage(),
    ),
    (route) => false,
  );
}

textToInteger(var va) {
  if (va.runtimeType == String) {
    return int.tryParse(va) ?? 0;
  }
  return va;
}

var rng = new Random();

// MaterialColor mainColor =
//     Colors.primaries[rng.nextInt(Colors.primaries.length)];
MaterialColor mainColor = Colors.orange;

Widget? startingPage;
List<TableRows> tableRows = [];
List<Items> selectedItems = [];
