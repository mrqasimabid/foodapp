import 'package:flutter/material.dart';
import 'package:foodapp/api-requests/requests.dart';
import 'package:foodapp/globels.dart';
import 'package:foodapp/models/Items.dart';
import 'package:foodapp/models/TableRow.dart';
import 'package:foodapp/pages/ItemDetail.dart';

class LayoutExample extends StatefulWidget {
  @override
  _LayoutExampleState createState() => _LayoutExampleState();
}

class _LayoutExampleState extends State<LayoutExample> {
  late List<Items> items;
  String selectedCategory = "all";

  bool loading = true;
  @override
  void initState() {
    getItems()
        .then((value) => setState(() {
              items = value;
              selectedItems = items;
            }))
        .whenComplete(() => setState(() {
              loading = false;
            }));
    // FutureBuilder(
    //   future: getItems(),
    //   builder: (context, AsyncSnapshot<dynamic> snapshot) {
    //     setState(() {
    //       items = snapshot.data;
    //       loading = false;
    //     });
    //   },
    // );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    // Orientation orientation = MediaQuery.of(context).orientation;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('POS ${width.toString()}'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: (loading)
            ? Container(child: Center(child: CircularProgressIndicator()))
            : Container(
                color: mainColor.shade200,
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [Text("hahahah")],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          height: height,
                          width: width,
                          child: GridView.count(
                            crossAxisSpacing: 30,
                            scrollDirection: Axis.vertical,
                            mainAxisSpacing: 30,
                            shrinkWrap: true,
                            crossAxisCount: (width < 780) ? 1 : 2,
                            children: [
                              Container(
                                child: Column(
                                  children: [
                                    Text("Row1"),
                                    Padding(
                                      padding: const EdgeInsets.all(28.0),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.elliptical(100, 200)),
                                          color: mainColor.shade300,
                                        ),
                                        child: SizedBox(
                                          width: width,
                                          height: 100,
                                          child: Row(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    selectedCategory = "all";
                                                    selectedItems = items;
                                                  });
                                                },
                                                child: new Chip(
                                                  backgroundColor:
                                                      (selectedCategory ==
                                                              "all")
                                                          ? mainColor.shade500
                                                          : mainColor.shade200,
                                                  label: Text("All"),
                                                ),
                                              ),
                                              Expanded(
                                                child: ListView.builder(
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemCount: items.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return InkWell(
                                                      onTap: () {
                                                        selectedCategory =
                                                            items[index].catid!;
                                                        print(items[index]
                                                            .categoryName!
                                                            .toLowerCase());
                                                        sortTable(items[index]
                                                            .catid!);
                                                      },
                                                      child: new Chip(
                                                        backgroundColor:
                                                            (selectedCategory ==
                                                                    items[index]
                                                                        .catid)
                                                                ? mainColor
                                                                    .shade500
                                                                : mainColor
                                                                    .shade200,
                                                        label: Text(items[index]
                                                            .categoryName!),
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    (selectedItems.isEmpty)
                                        ? Container(
                                            child: Center(
                                                child: Text(
                                                    "No Data Of this Category")))
                                        : Expanded(
                                            child: GridView.builder(
                                              scrollDirection: Axis.vertical,
                                              itemCount: selectedItems.length,
                                              gridDelegate:
                                                  SliverGridDelegateWithFixedCrossAxisCount(
                                                      crossAxisCount:
                                                          (width < 780)
                                                              ? 3
                                                              : 6),
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return InkWell(
                                                  onTap: () {
                                                    addToTable(
                                                        selectedItems[index]);
                                                  },
                                                  child: new Card(
                                                    child: new Container(
                                                      child: Image.network(
                                                          itemImage +
                                                              selectedItems[
                                                                      index]
                                                                  .photo!),
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                  ],
                                ),
                              ),
                              // SizedBox(
                              // height: height,
                              // child:
                              SingleChildScrollView(
                                physics: ScrollPhysics(
                                    parent: AlwaysScrollableScrollPhysics()),
                                child: Container(
                                  child: Column(
                                    children: [
                                      (tableRows.isEmpty)
                                          ? Container()
                                          : Container(
                                              padding: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                  // shape: BoxShape.circle,
                                                  borderRadius:
                                                      // BorderRadius.circular(50),
                                                      BorderRadius.only(
                                                    topLeft:
                                                        Radius.circular(100),
                                                    bottomLeft:
                                                        Radius.circular(100),
                                                  ),
                                                  color: mainColor.shade600),
                                              // color: ,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(28.0),
                                                child: DataTable(
                                                  horizontalMargin: 0,
                                                  dataRowHeight: 30,
                                                  columnSpacing: 15,
                                                  columns: [
                                                    DataColumn(
                                                      label: Text(
                                                        "Name",
                                                        // softWrap: true,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 20),
                                                      ),
                                                      numeric: false,
                                                    ),
                                                    DataColumn(
                                                      label: Text(
                                                        "Quantity",
                                                        softWrap: true,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 20),
                                                      ),
                                                      numeric: false,
                                                    ),
                                                    DataColumn(
                                                      label: Text(
                                                        "Action",
                                                        softWrap: true,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 20),
                                                      ),
                                                      numeric: false,
                                                    ),
                                                    DataColumn(
                                                      label: Text(
                                                        "$sum\nTotal",
                                                        softWrap: true,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 20),
                                                      ),
                                                      numeric: false,
                                                    ),
                                                  ],
                                                  rows: tableRows
                                                      .map(
                                                        (item) => DataRow(
                                                            cells: [
                                                              DataCell(
                                                                Container(
                                                                  width: 80,
                                                                  height: 70,
                                                                  child: Wrap(
                                                                    children: [
                                                                      Text(
                                                                        item.item!.itemDes ??
                                                                            '',
                                                                        softWrap:
                                                                            true,
                                                                        overflow:
                                                                            TextOverflow.visible,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                12),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                onTap: () {
                                                                  Navigator.push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              ItemDetail(item: item.item)));
                                                                },
                                                              ),
                                                              DataCell(
                                                                Container(
                                                                  width: 30,
                                                                  child: Text(
                                                                    item.qty
                                                                        .toString(),
                                                                    softWrap:
                                                                        true,
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            15),
                                                                  ),
                                                                ),
                                                              ),
                                                              DataCell(
                                                                Container(
                                                                  width: 75,
                                                                  child: Wrap(
                                                                    children: [
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          addToTable(
                                                                              item.item!);
                                                                        },
                                                                        child: Icon(
                                                                            Icons.add),
                                                                      ),
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          addToTable(
                                                                              item.item!,
                                                                              flag: false);
                                                                        },
                                                                        child: Icon(
                                                                            Icons.remove),
                                                                      ),
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          setState(
                                                                              () {
                                                                            tableRows.remove(item);
                                                                            calculateSum();
                                                                          });
                                                                        },
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .delete,
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              DataCell(
                                                                Container(
                                                                  width: 40,
                                                                  child: Text(
                                                                      "${(item.qty ?? 0) * int.parse(item.item!.cost ?? '0')}"),
                                                                ),
                                                              ),
                                                            ]),
                                                      )
                                                      .toList(),
                                                ),
                                              ),
                                            ),
                                    ],
                                  ),
                                ),
                              ),
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  addToTable(Items i, {bool flag = true}) {
    TableRows row = new TableRows(item: i, qty: 1);
    if (flag == false) {
      row.qty = -1;
    }

    if (tableRows.isNotEmpty) {
      List<TableRows> tr =
          tableRows.where((element) => element.item == i).toList();
      if (tr.isNotEmpty) {
        if (tr[0].qty! < 2 && row.qty == -1) {
          tableRows.remove(tr[0]);
        }
        setState(() {
          if (tr[0].qty != null) tr[0].qty = (tr[0].qty ?? 0) + (row.qty ?? 0);
        });
      } else {
        addRowToTable(row);
      }
    } else {
      addRowToTable(row);
    }
    calculateSum();
  }

  int sum = 0;
  addRowToTable(TableRows row) {
    setState(() {
      tableRows.add(row);
    });
  }

  sortTable(String categoryID) {
    setState(() {
      selectedItems =
          items.where((element) => element.catid == categoryID).toList();
    });
    print(selectedItems);
  }

  calculateSum() {
    setState(() {
      sum = 0;
      tableRows.forEach((element) {
        sum += int.parse(element.item!.cost ?? '0') * (element.qty ?? 0);
      });
    });
  }
}
