import 'package:flutter/material.dart';
import 'package:foodapp/models/Items.dart';
import 'package:foodapp/globels.dart';

class ItemDetail extends StatefulWidget {
  final Items? item;

  const ItemDetail({Key? key, this.item}) : super(key: key);
  @override
  _ItemDetailState createState() => _ItemDetailState();
}

class _ItemDetailState extends State<ItemDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
          child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15),
            width: double.infinity,
            color: Theme.of(context).backgroundColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  maxRadius: 100,
                  backgroundImage: (widget.item!.photo == null
                      ? null
                      : NetworkImage(itemImage + (widget.item!.photo ?? ''))),
                ),
                SizedBox(height: 10),
                Text(
                  widget.item!.itemDes!,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ],
            ),
          ),
          Text(widget.item!.photo ?? ''),
          // Container(
          //   child: Column(
          //     children: [
          //       ExpansionTile(
          //         title: Text('Basic'),
          //         leading: Icon(Icons.verified_user_rounded),
          //         children: [
          //           Row(children: [
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("CNIC:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text((widget.item.cnic == null)
          //                     ? "-"
          //                     : widget.item.cnic),
          //               ),
          //             ),
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("item Code:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text((widget.item.drcode == null)
          //                     ? "-"
          //                     : widget.item.drcode),
          //               ),
          //             )
          //           ]),
          //           Row(children: [
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("First Name:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text((widget.item.firstname == null)
          //                     ? "-"
          //                     : widget.item.firstname),
          //               ),
          //             ),
          //             SizedBox(
          //               width: 10,
          //             ),
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("Last Name:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text((widget.item.lastname == null)
          //                     ? "-"
          //                     : widget.item.lastname),
          //               ),
          //             )
          //           ]),
          //           ListTile(
          //             leading: Text("Date of Birth:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text(
          //                 (widget.item.dob == null) ? "-" : widget.item.dob),
          //           ),
          //           ListTile(
          //             leading: Text("Place of Work:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.placeofwork == null)
          //                 ? "-"
          //                 : widget.item.placeofwork),
          //           ),
          //         ],
          //       ),
          //       ExpansionTile(
          //         title: Text('Contact'),
          //         leading: Icon(Icons.contact_page),
          //         children: [
          //           ListTile(
          //             leading: Text("Mobile:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.mobile == null)
          //                 ? "-"
          //                 : widget.item.mobile),
          //           ),
          //           ListTile(
          //             leading: Text("Contact:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.contactno == null)
          //                 ? "-"
          //                 : widget.item.contactno),
          //           ),
          //           ListTile(
          //             leading: Text("Email:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.email == null)
          //                 ? "-"
          //                 : widget.item.email),
          //           ),
          //         ],
          //       ),
          //       ExpansionTile(
          //         title: Text('Address'),
          //         leading: Icon(Icons.home_filled),
          //         children: [
          //           ListTile(
          //             leading: Text("City:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.city == null)
          //                 ? "-"
          //                 : widget.item.city),
          //           ),
          //           ListTile(
          //             leading: Text("Address:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.address == null)
          //                 ? "-"
          //                 : widget.item.address),
          //           ),
          //         ],
          //       ),
          //       ExpansionTile(
          //         title: Text('Qualification'),
          //         leading: Icon(Icons.book),
          //         children: [
          //           ListTile(
          //             leading: Text("Graduation Institute:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.institute == null)
          //                 ? "-"
          //                 : widget.item.institute),
          //           ),
          //           Row(children: [
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("Qualification:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text(
          //                     (widget.item.postgradqualification == null)
          //                         ? "-"
          //                         : widget.item.postgradqualification),
          //               ),
          //             ),
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("Year:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text((widget.item.yearofgraduation == null)
          //                     ? "-"
          //                     : widget.item.yearofgraduation),
          //               ),
          //             )
          //           ]),
          //           ListTile(
          //             leading: Text("Post Graduation Institute:",
          //                 style: TextStyle(fontWeight: FontWeight.bold)),
          //             title: Text((widget.item.postgradinstitute == null)
          //                 ? "-"
          //                 : widget.item.postgradinstitute),
          //           ),
          //           Row(children: [
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("Qualification:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text(
          //                     (widget.item.postgradqualification == null)
          //                         ? "-"
          //                         : widget.item.postgradqualification),
          //               ),
          //             ),
          //             Expanded(
          //               child: ListTile(
          //                 leading: Text("Year:",
          //                     style: TextStyle(fontWeight: FontWeight.bold)),
          //                 title: Text((widget.item.postgradyear == null)
          //                     ? "-"
          //                     : widget.item.postgradyear),
          //               ),
          //             )
          //           ]),
          //         ],
          //       ),
          //       ExpansionTile(
          //         title: Text('Purpose of Visit'),
          //         leading: Icon(Icons.public_rounded),
          //         children: [
          //           Wrap(
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.all(10),
          //                 color: (widget.item.purposeofvisit == "1")
          //                     ? Colors.lightGreen
          //                     : null,
          //                 child: Center(
          //                   child: Text('Government Duty'),
          //                 ),
          //               ),
          //               Container(
          //                 padding: EdgeInsets.all(10),
          //                 color: (widget.item.purposeofvisit == "2")
          //                     ? Colors.lightBlue
          //                     : null,
          //                 child: Center(
          //                   child: Text('Volunteer'),
          //                 ),
          //               ),
          //               Container(
          //                 padding: EdgeInsets.all(10),
          //                 color: (widget.item.purposeofvisit == "3")
          //                     ? Colors.yellow
          //                     : null,
          //                 child: Center(
          //                   child: Text('Learning Purpose'),
          //                 ),
          //               ),
          //             ],
          //           ),
          //         ],
          //       ),
          //     ],
          //   ),
          // ),
          Container(),
        ],
      )),
    );
  }
}
