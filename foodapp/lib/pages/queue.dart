import 'dart:async';

import 'package:flutter/material.dart';
import 'package:foodapp/api-requests/requests.dart';
import 'package:foodapp/models/Tables.dart';

int kMobileBreakpoint = 576;
int kTabletBreakpoint = 1024;
int kDesktopBreakPoint = 1366;
int getColumns(width) {
  if (width <= kMobileBreakpoint) {
    return 3;
  } else if (width > kMobileBreakpoint && width <= kTabletBreakpoint) {
    return 4;
  } else if (width > kTabletBreakpoint && width <= kDesktopBreakPoint) {
    return 6;
  } else {
    return 6;
  }
}

BoxDecoration dec = const BoxDecoration(
  // gradient: RadialGradient(colors: colors)

  gradient: LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
    colors: [
      Colors.white,
      Colors.blue,
      Colors.purple,
      Colors.white,
      Colors.white,
      Colors.grey,
      Colors.orange,
      Colors.white,
    ],
  ),
);

class QueueManagement extends StatefulWidget {
  const QueueManagement({Key? key}) : super(key: key);

  @override
  _QueueManagementState createState() => _QueueManagementState();
}

class _QueueManagementState extends State<QueueManagement> {
  late final Timer _timer;
  // new Timer.periodic(oneSecond, (Timer t) => setState((){}));
  @override
  void initState() {
    _timer = Timer.periodic(Duration(seconds: 5), (Timer t) {
      if (mounted) {
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int width = getColumns(MediaQuery.of(context).size.width);
    return Scaffold(
        appBar: AppBar(),
        body: Container(
          // decoration: dec,
          padding: const EdgeInsets.all(12.0),
          child: FutureBuilder(
            future: fetchAllTables(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return LinearProgressIndicator();
              } else {
                List<Tables> tables = snapshot.data as List<Tables>;
                return GridView.builder(
                  itemCount: tables.length,
                  // itemCount: images.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: width,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 10.0),
                  itemBuilder: (BuildContext context, int index) {
                    Tables table = tables[index];
                    return InkWell(
                      onTap: () {
                        // tables[index]. = !images[index]['Status'];
                        setState(() {});
                      },
                      child: GridTile(
                        footer: Material(
                          color: Colors.transparent,
                          clipBehavior: Clip.hardEdge,
                          child: GridTileBar(
                            backgroundColor: Colors.black,
                            title: Text(
                              "${table.tableId.toString()}",
                              style: const TextStyle(fontSize: 30.0),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        header: Material(
                          color: Colors.transparent,
                          clipBehavior: Clip.hardEdge,
                          child: GridTileBar(
                            backgroundColor: Colors.black87,
                            title: Text(
                              table.tokenno == "0"
                                  ? returnColorandText(
                                      table.tokenno, table.active)['text']
                                  : "T#: ${table.tokenno.toString()}",
                              style: const TextStyle(fontSize: 20.0),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        child: Icon(Icons.chair,
                            size: 30.0 * width,
                            color: returnColorandText(
                                table.tokenno, table.active)['color']),
                      ),
                    );
                  },
                );
              }
            },
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    print("Queue Page dispose");
  }

  returnColorandText(token, active) {
    Color clr;
    String text;
    if (active == "0") {
      clr = Colors.grey;
      text = "Closed";
    } else {
      if (token == "0") {
        clr = Colors.green;
        text = "Available";
      } else {
        clr = Colors.red;
        text = "Main kisi or ka hoon filhal";
      }
    }
    return {'color': clr, 'text': text};
  }
}
