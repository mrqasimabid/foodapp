import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:pdf/pdf.dart';
import 'package:path_provider/path_provider.dart';
import 'package:printing/printing.dart';
import 'package:share/share.dart';

class PDFView extends StatefulWidget {
  final Uint8List url;
  final String type;

  const PDFView({Key? key, required this.url, this.type = 'report'})
      : super(key: key);
  @override
  _PDFViewState createState() => _PDFViewState();
}

// var _scaffoldKey = new GlobalKey<ScaffoldState>();

class _PDFViewState extends State<PDFView> {
  bool _isLoading = true;
  late File file;
  late final pdfController;
  PDFDocument document = new PDFDocument();

  @override
  void initState() {
    super.initState();
    loadDocument();
  }

  loadDocument() async {
    // File file = File.fromRawPath(widget.url);
    //Get directory
    Directory directory = await getApplicationDocumentsDirectory();
    //Get directory path
    String path = directory.path;
    //Create an empty file to write PDF data
    String filePath =
        '$path/' + (widget.type == 'report' ? 'Report.pdf' : 'Invoice.pdf');

    file = File(filePath);
    bool isFileExist = await file.exists();
    print(isFileExist);
    if (isFileExist) await new File(filePath).delete();
    await new File(filePath).create(recursive: true);

    try {
      //Write PDF data
      await file.writeAsBytes(widget.url, flush: true);
      //Open the PDF document in mobile
      file = File(filePath);
      document = await PDFDocument.fromFile(file);
      // pdfController = NativePDF.PdfController(
      //   document: NativePDF.PdfDocument.openFile(filePath),
      // );
      // print(document.count.toString());
      // if (document.count < 1) Navigator.pop(context, "No Data Found!!!");
    } catch (err) {
      Navigator.pop(context, err.toString());
    }
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text((widget.type == 'report' ? 'Report' : 'Invoice')),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          ButtonBar(
            buttonMinWidth: 0,
            children: [
              TextButton(
                  onPressed: () async {
                    await Printing.layoutPdf(
                        onLayout: (PdfPageFormat format) async => widget.url);
                  },
                  child: Icon(
                    Icons.print,
                    color: Colors.white,
                  )),
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          //Get directory
          Directory directory = await getApplicationDocumentsDirectory();
          //Get directory path
          String path = directory.path;
          Share.shareFiles([
            '$path/' + (widget.type == 'report' ? 'Report.pdf' : 'Invoice.pdf')
          ]);
        },
        child: Icon(Icons.share_outlined),
        backgroundColor: Colors.green,
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Container(
              child: PDFViewer(
                document: document,
                zoomSteps: 2,
                // uncomment below line to scroll vertically
                // scrollDirection: Axis.vertical,

                //uncomment below code to replace bottom navigation with your own
                /* navigationBuilder:
                      (context, page, totalPages, jumpToPage, animateToPage) {
                    return ButtonBar(
                      alignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.first_page),
                          onPressed: () {
                            jumpToPage()(page: 0);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () {
                            animateToPage(page: page - 2);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_forward),
                          onPressed: () {
                            animateToPage(page: page);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.last_page),
                          onPressed: () {
                            jumpToPage(page: totalPages - 1);
                          },
                        ),
                      ],
                    );
                  }, */
              ),
            ),
    );
  }
}
