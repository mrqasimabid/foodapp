import 'package:flutter/material.dart';
import 'package:foodapp/models/Items.dart';
import '../globels.dart';
import '../models/Login.dart';
import 'loginPage.dart';

// ignore: must_be_immutable
class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, this.title, this.user}) : super(key: key);
  final String? title;
  final Login? user;
  // MenuFull menus;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Items> selectedItem = [];
  bool flag = false;
  @override
  void initState() {
    super.initState();
    // widget.menus = menuFullFromMap(widget.user.desc.substring(8));
  }

  toggleView() {
    setState(() {
      view = !view;
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        // checkDRNO();
      });
    });
  }

  bool view = false;
  @override
  Widget build(BuildContext context) {
    // setColor(int i) {
    //   setState(() {
    //     mainColor = Colors.primaries[i];
    //     print(mainColor);
    //   });
    // }

    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor.shade200,
        appBar: AppBar(
            title: Text(widget.title ?? ''),
            centerTitle: true,
            actions: [
              Padding(
                padding: EdgeInsets.all(8),
                child: InkWell(
                  child: Icon(Icons.logout),
                  onTap: () {
                    logout(context);

                    setState(() {
                      startingPage = LoginPage();
                    });
                  },
                ),
              ),
            ]),
        // drawer: Draw(context, widget.user),
        drawer: Drawer(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                "Welcome: " + widget.user!.uname!,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
