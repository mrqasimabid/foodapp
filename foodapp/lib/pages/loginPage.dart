import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../api-requests/requests.dart';
import '../globels.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key, this.title}) : super(key: key);
  final String? title;
  @override
  _LoginForm createState() => _LoginForm();
}

class _LoginForm extends State<LoginPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool loading = true;
  final TextEditingController uname = new TextEditingController();
  final TextEditingController pass = new TextEditingController();

  @override
  void initState() {
    super.initState();
    uname.clear();
    pass.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Login Page"),
        centerTitle: true,
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth > 600) {
            return _buildTabDisplay();
          } else {
            return _buildMobileDisplay();
          }
        },
      ),

      // drawer: Draw(context),
      backgroundColor: Colors.grey[50],
    );
  }

  bool _obscureText = true;
  togglePassword() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  Widget _buildMobileDisplay() {
    return SafeArea(
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 100),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        // width: width / 2,
                        child: new Image.asset(
                          'assets/logo.png',
                          height: 50,
                        ),
                      ),
                    ]),
              ),
              _form(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTabDisplay() {
    return SafeArea(
      child: Center(
        child: Container(
          constraints: BoxConstraints(
            maxWidth: 600,
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        // width: width / 2,
                        child: new Image.asset(
                          'assets/logo.png',
                          height: 50,
                        ),
                      ),
                    ]),
                _form(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _form(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: Column(
        children: [
          Container(
            // color: Theme.of(context).accentColor,
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: FormBuilder(
                key: _fbKey,
                child: Column(
                  children: <Widget>[
                    FormBuilderTextField(
                      controller: uname,
                      name: "uname",
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 4.5, horizontal: 8),
                        // icon: const Padding(
                        //   padding: const EdgeInsets.only(top: 15.0),
                        //   child: const Icon(Icons.person),
                        // ),
                        labelText: 'Username',
                        border: new OutlineInputBorder(
                          gapPadding: 2.0,
                          borderRadius: new BorderRadius.circular(5.0),
                          borderSide:
                              BorderSide(color: Colors.grey, width: 1.5),
                        ),
                      ),
                      validator: FormBuilderValidators.required(context),
                      // focusNode: inputOne,
                    ),
                    SizedBox(height: 15),
                    FormBuilderTextField(
                      controller: pass,
                      name: "pass",
                      decoration: InputDecoration(
                        labelText: "Password",
                        // icon: const Padding(
                        //     padding: const EdgeInsets.only(top: 15.0),
                        //     child: const Icon(Icons.lock)),
                        suffixIcon: InkWell(
                            child: Icon(_obscureText
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onTap: (() {
                              togglePassword();
                            })),
                        border: new OutlineInputBorder(
                          gapPadding: 2.0,
                          borderRadius: new BorderRadius.circular(5.0),
                          borderSide:
                              BorderSide(color: Colors.grey, width: 1.5),
                        ),
                      ),
                      validator: FormBuilderValidators.required(context),
                      obscureText: _obscureText,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MaterialButton(
                          onPressed: () => {submitform(context)},
                          child: Text(
                            'Login',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Theme.of(context).colorScheme.secondary,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  submitform(BuildContext context) async {
    if (_fbKey.currentState!.saveAndValidate()) {
      showSnackbar(_scaffoldKey, "Logging In...", "wait");

      String username = uname.text;
      String password = pass.text;
      var login = await loginUser(username, password);
      // print(login);
      if (login == true) {
        showSnackbar(_scaffoldKey, "Login Success", "success");

        Navigator.of(context).pop();
        Navigator.of(context).pushNamed('/order-dashboard');
      } else {
        showSnackbar(_scaffoldKey, "Wrong Username or Password.", "fail");
      }
    }
  }
}
