import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:foodapp/api-requests/requests.dart';
import 'package:foodapp/globels.dart';
import 'package:foodapp/models/Items.dart';
import 'package:foodapp/models/KotDetail.dart';
import 'package:foodapp/models/KotMain.dart';
import 'package:foodapp/models/TableRow.dart';
import 'package:foodapp/pages/ItemDetail.dart';
import 'package:foodapp/pages/loginPage.dart';
import 'package:foodapp/pages/pdfViewer.dart';
import 'package:foodapp/pages/queue.dart';

class StackImplementation extends StatefulWidget {
  @override
  _StackImplementationState createState() => _StackImplementationState();
}

class _StackImplementationState extends State<StackImplementation>
    with SingleTickerProviderStateMixin {
  List<Items> items = [];
  String selectedCategory = "all";
  String maxVrno = '0';
  bool loading = true;
  bool showloader = false;
  String vouchertype = "new";
  String currentOrder = "";
  final TextEditingController vrnoaController = new TextEditingController();
  int _currentIndex = 0;
  bool topBarShow = false;
  @override
  void initState() {
    getItems()
        .then((value) => setState(() {
              items = value;

              selectedItems = items;
            }))
        .whenComplete(() => setState(() {
              loading = false;
            }));

    getMaxVrno()
        .then((value) => setState(() {
              maxVrno = value;
              selectedItems = items;
              vrnoaController.text = maxVrno;
            }))
        .whenComplete(() => setState(() {
              loading = false;
            }));

    // getMaxVrno().then((value) => maxVrno = value);

    super.initState();
  }

  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    // Orientation orientation = MediaQuery.of(context).orientation;
    double appbarheight = AppBar().preferredSize.height;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height - appbarheight;
    // double halfheight = height / 2;
    double fat = 80;

    return DefaultTabController(
      length: 1,
      initialIndex: 0,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          actions: [
            Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.chair,
                    color: Colors.white,
                  ),
                  tooltip: "Queue",
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => QueueManagement()),
                    );
                  },
                ),
                IconButton(
                  icon: Icon(
                    Icons.refresh,
                    color: Colors.white,
                  ),
                  tooltip: "Refresh",
                  onPressed: () async {
                    FocusScopeNode currentScope = FocusScope.of(context);
                    if (!currentScope.hasPrimaryFocus &&
                        currentScope.hasFocus) {
                      FocusManager.instance.primaryFocus!.unfocus();
                    }
                    maxVrno = await getMaxVrno();
                    vrnoaSave = maxVrno;
                    vrnoaController.text = maxVrno;
                    vouchertype = "new";
                    currentOrder = "";
                    tableRows = [];
                    setState(() {});
                  },
                ),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  tooltip: "Logout",
                  onPressed: () {
                    logout(context);

                    setState(() {
                      startingPage = LoginPage();
                    });
                  },
                ),
              ],
            )
          ],
          title: Text(
            "Order Placement",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          bottom: TabBar(
            labelColor: Colors.white,
            labelStyle: TextStyle(fontSize: 16),
            indicatorColor: Colors.white,
            tabs: [
              Tab(text: 'New Order'),
              // Tab(text: 'View All'),
            ],
          ),
        ),
        body: (loading)
            ? Container(
                height: MediaQuery.of(context).size.height - 80,
                child: Center(child: CircularProgressIndicator()))
            : TabBarView(
                children: [
                  Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 8.0, right: 8.0, top: 15, bottom: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Visibility(
                                  visible: topBarShow,
                                  child: Expanded(
                                    flex: 2,
                                    child: new FormBuilderTextField(
                                      keyboardType: TextInputType.number,
                                      name: "vr#",
                                      controller: vrnoaController,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 18),
                                      decoration: InputDecoration(
                                        labelStyle: TextStyle(fontSize: 18),
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 4.5, horizontal: 8),
                                        labelText: "VR #",
                                        border: new OutlineInputBorder(
                                          gapPadding: 2.0,
                                          borderRadius:
                                              new BorderRadius.circular(5.0),
                                          borderSide: BorderSide(
                                              color: Colors.grey, width: 1.5),
                                        ),
                                        prefix: IconButton(
                                          alignment: Alignment.bottomCenter,
                                          constraints:
                                              BoxConstraints(maxHeight: 30),
                                          icon: Icon(Icons.remove, size: 18),
                                          onPressed: () {
                                            var vrnoa = textToInteger(
                                                vrnoaController.text);
                                            if (vrnoa >= 2) {
                                              vrnoaController.text =
                                                  (vrnoa - 1).toString();
                                              fetchKotbyID(
                                                  vrnoaController.text);
                                            }
                                          },
                                        ),
                                        suffix: IconButton(
                                          alignment: Alignment.bottomCenter,
                                          constraints:
                                              BoxConstraints(maxHeight: 30),
                                          icon: Icon(Icons.add, size: 18),
                                          onPressed: () {
                                            var vrnoa = textToInteger(
                                                vrnoaController.text);
                                            vrnoaController.text =
                                                (vrnoa + 1).toString();
                                            fetchKotbyID(vrnoaController.text);
                                          },
                                        ),
                                      ),
                                      validator: FormBuilderValidators.required(
                                          context),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15),
                                Visibility(
                                  visible: topBarShow,
                                  child: Expanded(
                                    flex: 1,
                                    child: ElevatedButton(
                                      onPressed: () {
                                        fetchKotbyID(vrnoaController.text);
                                      },
                                      child: Text(
                                        'View',
                                        style: TextStyle(
                                            fontSize: (width < 800 ? 16 : 18)),
                                      ),
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Colors.grey[800]),
                                        foregroundColor:
                                            MaterialStateProperty.all(
                                                Colors.white),
                                        padding: MaterialStateProperty.all(
                                          EdgeInsets.symmetric(
                                              vertical: 14, horizontal: 8),
                                        ),
                                        fixedSize: MaterialStateProperty.all(
                                            Size.fromWidth(100)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15),
                                Expanded(
                                  flex: 1,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      saveOrder();
                                    },
                                    child: Text(
                                      'Place Order',
                                      style: TextStyle(
                                          fontSize: (width < 800 ? 16 : 18)),
                                    ),
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(mainColor),
                                      foregroundColor:
                                          MaterialStateProperty.all(
                                              Colors.white),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.symmetric(
                                              vertical: 14, horizontal: 8)),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15),
                                Visibility(
                                  visible: topBarShow,
                                  child: Expanded(
                                    flex: 1,
                                    child: ElevatedButton(
                                      onPressed: () async {
                                        if (vrnoaController.text != "" &&
                                            vrnoaSave != maxVrno) {
                                          var data = await fetchPrint(
                                              vrnoa: vrnoaController.text);
                                          final result = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  PDFView(
                                                url: data,
                                              ),
                                            ),
                                          );
                                          if (result != null) {
                                            showSnackbar(
                                                _scaffoldKey,
                                                'Failed to load print.',
                                                "false");
                                          }
                                        }
                                      },
                                      child: Text(
                                        'Print Bill',
                                        style: TextStyle(
                                            fontSize: (width < 800 ? 16 : 18)),
                                      ),
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Colors.blue),
                                        foregroundColor:
                                            MaterialStateProperty.all(
                                                Colors.white),
                                        padding: MaterialStateProperty.all(
                                            EdgeInsets.symmetric(
                                                vertical: 14, horizontal: 8)),
                                      ),
                                    ),
                                  ),
                                ),
                                if (width > 800)
                                  Expanded(flex: 2, child: Container()),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                // height: height,
                                width: width,
                                child: GridView.count(
                                  crossAxisCount: (width <= 800) ? 1 : 2,
                                  crossAxisSpacing: 20,
                                  mainAxisSpacing: 20,
                                  shrinkWrap: true,
                                  children: [
                                    Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              height: fat,
                                              width: (width <= 800)
                                                  ? (width - 20)
                                                  : (width / 2) - 20,
                                              child: Row(
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        selectedCategory =
                                                            "all";
                                                        selectedItems = items;
                                                      });
                                                    },
                                                    child: new Container(
                                                      decoration: BoxDecoration(
                                                        color:
                                                            mainColor.shade500,
                                                        border: (selectedCategory ==
                                                                "all")
                                                            ? Border.all(
                                                                color: Colors
                                                                    .black,
                                                                width: 2.0,
                                                                style:
                                                                    BorderStyle
                                                                        .solid)
                                                            : null,
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                vertical: 20.0,
                                                                horizontal: 20),
                                                        child: Text(
                                                          "All",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 18),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: ListView.builder(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      itemCount: items.length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return InkWell(
                                                          onTap: () {
                                                            selectedCategory =
                                                                items[index]
                                                                    .catid!;
                                                            // print(items[index]
                                                            //     .categoryName!
                                                            //     .toLowerCase());
                                                            sortTable(
                                                                items[index]
                                                                    .catid!);
                                                          },
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                                    border:
                                                                        Border
                                                                            .all(
                                                              width: 3.0,
                                                              color: (selectedCategory ==
                                                                      items[index]
                                                                          .catid)
                                                                  ? Colors.black
                                                                  : mainColor
                                                                      .shade50,
                                                            )),
                                                            child: items[index]
                                                                    .photo!
                                                                    .isEmpty
                                                                ? new Image
                                                                        .asset(
                                                                    'assets/barber.jpeg')
                                                                : new Image
                                                                        .network(
                                                                    itemImage +
                                                                        items[index]
                                                                            .photo!),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        // Positioned(
                                        //   top: fat,
                                        //   child: Column(
                                        //     children: [
                                        //       Container(
                                        //         width: fat,
                                        //         height: height - fat,
                                        //         child: Column(
                                        //           children: [
                                        //             Expanded(
                                        //               child: ListView.builder(
                                        //                 scrollDirection:
                                        //                     Axis.vertical,
                                        //                 itemCount: items.length,
                                        //                 itemBuilder:
                                        //                     (BuildContext context,
                                        //                         int index) {
                                        //                   return InkWell(
                                        //                     onTap: () {
                                        //                       selectedCategory =
                                        //                           items[index].catid!;
                                        //                       // print(items[index]
                                        //                       //     .categoryName!
                                        //                       //     .toLowerCase());
                                        //                       sortTable(items[index]
                                        //                           .catid!);
                                        //                     },
                                        //                     child: Container(
                                        //                       decoration:
                                        //                           BoxDecoration(
                                        //                               border:
                                        //                                   Border.all(
                                        //                         width: 3,
                                        //                         color: (selectedCategory ==
                                        //                                 items[index]
                                        //                                     .catid)
                                        //                             ? Colors.black
                                        //                             : mainColor
                                        //                                 .shade50,
                                        //                       )),
                                        //                       child: new Image
                                        //                               .network(
                                        //                           itemImage +
                                        //                               items[index]
                                        //                                   .photo!),
                                        //                     ),
                                        //                   );
                                        //                 },
                                        //               ),
                                        //             )
                                        //           ],
                                        //         ),
                                        //       )
                                        //     ],
                                        //   ),
                                        // ),
                                        Flexible(
                                          flex: 1,
                                          child: Container(
                                            width: (width <= 800)
                                                ? (width - 20)
                                                : (width - fat),
                                            height: height,
                                            child: (selectedItems.isEmpty)
                                                ? Container(
                                                    child: Center(
                                                      child: Text(
                                                          "No Data Of this Category"),
                                                    ),
                                                  )
                                                : Column(
                                                    children: [
                                                      Flexible(
                                                        fit: FlexFit.loose,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  bottom: 20.0),
                                                          child:
                                                              GridView.builder(
                                                            physics: ScrollPhysics(
                                                                parent:
                                                                    AlwaysScrollableScrollPhysics()),
                                                            shrinkWrap: true,
                                                            scrollDirection:
                                                                Axis.vertical,
                                                            itemCount:
                                                                selectedItems
                                                                    .length,
                                                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                                                crossAxisSpacing:
                                                                    5.0,
                                                                mainAxisSpacing:
                                                                    5.0,
                                                                crossAxisCount:
                                                                    (width <
                                                                            780)
                                                                        ? 3
                                                                        : 3),
                                                            itemBuilder:
                                                                (BuildContext
                                                                        context,
                                                                    int index) {
                                                              return AnimatedOpacity(
                                                                duration:
                                                                    Duration(
                                                                        seconds:
                                                                            1),
                                                                opacity: 1.0,
                                                                child: GridTile(
                                                                  footer:
                                                                      Material(
                                                                    color: Colors
                                                                        .transparent,
                                                                    shape: const RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.vertical(bottom: Radius.circular(4))),
                                                                    clipBehavior:
                                                                        Clip.antiAlias,
                                                                    child:
                                                                        GridTileBar(
                                                                      backgroundColor:
                                                                          Colors
                                                                              .black54,
                                                                      title: Text(
                                                                          selectedItems[index].itemDes ??
                                                                              '',
                                                                          style:
                                                                              TextStyle(fontSize: 16.0)),
                                                                      subtitle: Text(
                                                                          selectedItems[index].categoryName ??
                                                                              '',
                                                                          style:
                                                                              TextStyle(fontSize: 12.0)),
                                                                      trailing:
                                                                          IconButton(
                                                                        padding:
                                                                            EdgeInsets.zero,
                                                                        tooltip:
                                                                            "View Item",
                                                                        icon: Icon(
                                                                            Icons.remove_red_eye),
                                                                        onPressed:
                                                                            () {
                                                                          showDialog<
                                                                              AlertDialog>(
                                                                            context:
                                                                                context,
                                                                            barrierDismissible:
                                                                                true,
                                                                            builder:
                                                                                (BuildContext context) {
                                                                              return AlertDialog(
                                                                                backgroundColor: Colors.transparent,
                                                                                content: StatefulBuilder(
                                                                                  builder: (BuildContext context, StateSetter setState) {
                                                                                    return Container(
                                                                                      width: (width <= 800) ? (width - 20) : (width / 2) - 20,
                                                                                      child: GridTile(
                                                                                        footer: Material(
                                                                                          color: Colors.transparent,
                                                                                          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.vertical(bottom: Radius.circular(4))),
                                                                                          clipBehavior: Clip.antiAlias,
                                                                                          child: GridTileBar(
                                                                                            backgroundColor: Colors.black87,
                                                                                            title: Text(selectedItems[index].itemDes ?? '', style: TextStyle(fontSize: 18.0)),
                                                                                            subtitle: Text(selectedItems[index].categoryName ?? '', style: TextStyle(fontSize: 14.0)),
                                                                                          ),
                                                                                        ),
                                                                                        child: items[index].photo!.isEmpty
                                                                                            ? new Image.asset(
                                                                                                'assets/barber.jpeg',
                                                                                                fit: BoxFit.cover,
                                                                                              )
                                                                                            : new Image.network(
                                                                                                itemImage + selectedItems[index].photo!,
                                                                                                fit: BoxFit.cover,
                                                                                              ),
                                                                                      ),
                                                                                    );
                                                                                  },
                                                                                ),
                                                                              );
                                                                            },
                                                                          );
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  child: InkWell(
                                                                      onTap: () {
                                                                        addToTable(
                                                                            selectedItems[index]);
                                                                      },
                                                                      child: Material(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius:
                                                                                BorderRadius.circular(4.0)),
                                                                        child: items[index].photo!.isEmpty
                                                                            ? new Image.asset(
                                                                                'assets/barber.jpeg',
                                                                                fit: BoxFit.cover,
                                                                              )
                                                                            : new Image.network(
                                                                                itemImage + selectedItems[index].photo!,
                                                                                fit: BoxFit.cover,
                                                                              ),
                                                                      )),
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SingleChildScrollView(
                                      child: Center(
                                        child: Column(
                                          children: [
                                            (showloader)
                                                ? LinearProgressIndicator()
                                                :
                                                // (tableRows.isEmpty)
                                                //     ? Container(
                                                //         height: fat,
                                                //         child: Center(
                                                //             child: Text(
                                                //           "Add Item to show Bill Detail...",
                                                //           style:
                                                //               TextStyle(fontSize: 16),
                                                //         )))
                                                //     :
                                                Container(
                                                    width: (width <= 800)
                                                        ? (width - 20)
                                                        : (width / 2) - 20,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10.0)),
                                                    ),
                                                    child: Card(
                                                      shape: RoundedRectangleBorder(
                                                          side: BorderSide(
                                                              width: 1.0,
                                                              color: mainColor),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius.circular(
                                                                      10.0))),
                                                      elevation: 2.0,
                                                      child: Column(
                                                        children: [
                                                          Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 20.0,
                                                                      left:
                                                                          20.0,
                                                                      right:
                                                                          20.0,
                                                                      bottom:
                                                                          8.0),
                                                              child:
                                                                  // (vrnoaSave !=
                                                                  //             null &&
                                                                  //         vrnoaSave !=
                                                                  //             maxVrno)
                                                                  //     ? InkWell(
                                                                  //         onTap: () {
                                                                  //           var data =
                                                                  //               jsonDecode(
                                                                  //                   currentOrder);
                                                                  //           print(
                                                                  //               data);
                                                                  //         },
                                                                  //         child: Row(
                                                                  //           mainAxisAlignment:
                                                                  //               MainAxisAlignment
                                                                  //                   .end,
                                                                  //           children: [
                                                                  //             Icon(Icons
                                                                  //                 .info),
                                                                  //             SizedBox(
                                                                  //               width:
                                                                  //                   5,
                                                                  //             ),
                                                                  //             Text(
                                                                  //                 "View Detail")
                                                                  //           ],
                                                                  //         ),
                                                                  //       )
                                                                  // :
                                                                  Container()),
                                                          Container(
                                                            width: (width <=
                                                                    800)
                                                                ? (width - 20)
                                                                : (width / 2) -
                                                                    20,
                                                            child: Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .only(
                                                                left: 20,
                                                                right: 20,
                                                              ),
                                                              child: DataTable(
                                                                horizontalMargin:
                                                                    10.0,
                                                                dataRowHeight:
                                                                    35,
                                                                columnSpacing:
                                                                    0.0,
                                                                headingRowColor:
                                                                    MaterialStateProperty.all(
                                                                        Colors.grey[
                                                                            200]),
                                                                headingRowHeight:
                                                                    40,
                                                                columns: [
                                                                  DataColumn(
                                                                    label: Text(
                                                                      "Name",
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            15,
                                                                      ),
                                                                    ),
                                                                    numeric:
                                                                        false,
                                                                  ),
                                                                  // DataColumn(
                                                                  //   label: Text(
                                                                  //     "Quantity",
                                                                  //     softWrap:
                                                                  //         true,
                                                                  //     overflow:
                                                                  //         TextOverflow
                                                                  //             .ellipsis,
                                                                  //     style:
                                                                  //         TextStyle(
                                                                  //       fontSize:
                                                                  //           15,
                                                                  //     ),
                                                                  //   ),
                                                                  //   numeric:
                                                                  //       false,
                                                                  // ),
                                                                  DataColumn(
                                                                    label: Text(
                                                                      "Rate",
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            15,
                                                                      ),
                                                                    ),
                                                                    numeric:
                                                                        false,
                                                                  ),
                                                                  // DataColumn(
                                                                  //   label: Text(
                                                                  //     "Amount",
                                                                  //     overflow:
                                                                  //         TextOverflow
                                                                  //             .ellipsis,
                                                                  //     style:
                                                                  //         TextStyle(
                                                                  //       fontSize:
                                                                  //           15,
                                                                  //     ),
                                                                  //   ),
                                                                  //   numeric:
                                                                  //       true,
                                                                  // ),
                                                                  DataColumn(
                                                                      label: Text(
                                                                          ""),
                                                                      numeric:
                                                                          true),
                                                                ],
                                                                rows: tableRows
                                                                    .map(
                                                                      (item) =>
                                                                          DataRow(
                                                                              cells: [
                                                                            DataCell(
                                                                              Text(
                                                                                item.item!.itemDes ?? "",
                                                                                softWrap: true,
                                                                                overflow: TextOverflow.visible,
                                                                                style: TextStyle(
                                                                                  fontSize: 14,
                                                                                ),
                                                                              ),
                                                                              // Container(
                                                                              //   constraints: BoxConstraints(minWidth: width / 5),
                                                                              //   child: Wrap(
                                                                              //     children: [
                                                                              //       Text(
                                                                              //         item.item!.itemDes ?? "",
                                                                              //         softWrap: true,
                                                                              //         overflow: TextOverflow.visible,
                                                                              //         style: TextStyle(
                                                                              //           fontSize: 14,
                                                                              //         ),
                                                                              //       ),
                                                                              //     ],
                                                                              //   ),
                                                                              // ),
                                                                              onTap: () {
                                                                                Navigator.push(context, MaterialPageRoute(builder: (context) => ItemDetail(item: item.item)));
                                                                              },
                                                                            ),
                                                                            // DataCell(
                                                                            //   Container(
                                                                            //     // width: 75,
                                                                            //     child: Row(
                                                                            //       children: (item.old ?? false)
                                                                            //           ? [
                                                                            //               Text(
                                                                            //                 "Placed(${item.qty.toString()})",
                                                                            //                 style: TextStyle(),
                                                                            //               )
                                                                            //             ]
                                                                            //           : [
                                                                            //               InkWell(
                                                                            //                 onTap: () {
                                                                            //                   addToTable(item.item!);
                                                                            //                 },
                                                                            //                 child: Icon(
                                                                            //                   Icons.add,
                                                                            //                   color: Colors.green[600],
                                                                            //                   size: 22,
                                                                            //                 ),
                                                                            //               ),
                                                                            //               SizedBox(width: 13),
                                                                            //               Text(
                                                                            //                 item.qty.toString(),
                                                                            //                 softWrap: true,
                                                                            //                 textAlign: TextAlign.center,
                                                                            //                 overflow: TextOverflow.ellipsis,
                                                                            //                 style: TextStyle(
                                                                            //                   fontSize: 15,
                                                                            //                 ),
                                                                            //               ),
                                                                            //               SizedBox(width: 13),
                                                                            //               InkWell(
                                                                            //                 onTap: () {
                                                                            //                   addToTable(item.item!, flag: false);
                                                                            //                 },
                                                                            //                 child: Icon(
                                                                            //                   Icons.remove,
                                                                            //                   color: Colors.red[600],
                                                                            //                   size: 22,
                                                                            //                 ),
                                                                            //               ),
                                                                            //             ],
                                                                            //     ),
                                                                            //   ),
                                                                            // ),
                                                                            DataCell(
                                                                              // Text("${item.item!.cost ?? '0'}")
                                                                              Container(
                                                                                width: 20,
                                                                                child: Text("${item.item!.cost ?? '0'}"),
                                                                              ),
                                                                            ),
                                                                            // DataCell(
                                                                            //   Text("${(item.qty ?? 0) * int.parse(item.item!.cost ?? '0')}"),

                                                                            //   // Container(
                                                                            //   //   // width: 20,
                                                                            //   //   child: Text("${(item.qty ?? 0) * int.parse(item.item!.cost ?? '0')}"),
                                                                            //   // ),
                                                                            // ),
                                                                            DataCell(
                                                                              InkWell(
                                                                                onTap: () {
                                                                                  setState(() {
                                                                                    tableRows.remove(item);
                                                                                    calculateSum();
                                                                                  });
                                                                                },
                                                                                child: Icon(Icons.delete_forever, color: Colors.red[900]),
                                                                              ),
                                                                              // Container(
                                                                              //   // width: 20,
                                                                              //   child: InkWell(
                                                                              //     onTap: () {
                                                                              //       setState(() {
                                                                              //         tableRows.remove(item);
                                                                              //         calculateSum();
                                                                              //       });
                                                                              //     },
                                                                              //     child: Icon(Icons.delete_forever, color: Colors.red[900]),
                                                                              //   ),
                                                                              // ),
                                                                            ),
                                                                          ]),
                                                                    )
                                                                    .toList(),
                                                              ),
                                                            ),
                                                          ),
                                                          Divider(),
                                                          Container(
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .only(
                                                                bottom: 20.0,
                                                                right: 20.0,
                                                                top: 6.0,
                                                              ),
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .end,
                                                                children: [
                                                                  if (tableRows
                                                                      .isNotEmpty)
                                                                    Text(
                                                                      "Total: ",
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight
                                                                              .w700,
                                                                          fontSize:
                                                                              18),
                                                                    ),
                                                                  if (tableRows
                                                                      .isNotEmpty)
                                                                    Text(
                                                                      sum.toString(),
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              18),
                                                                    ),
                                                                ],
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Container(
                  //   child: FutureBuilder(
                  //     future: _fetchAllOrdersList(),
                  //     builder: (context, AsyncSnapshot<dynamic> snapshot) {
                  //       List<Widget> children;
                  //       if (snapshot.hasData) {
                  //         var data = jsonDecode(snapshot.data);
                  //         children = <Widget>[
                  //           Expanded(
                  //             flex: 1,
                  //             child: Padding(
                  //               padding: const EdgeInsets.only(top: 16),
                  //               child: ListView.builder(
                  //                 scrollDirection: Axis.vertical,
                  //                 shrinkWrap: true,
                  //                 itemCount: data.length,
                  //                 itemBuilder: (context, index) {
                  //                   return Column(
                  //                     children: [
                  //                       ListTile(
                  //                         leading:
                  //                             Icon(Icons.local_dining_outlined),
                  //                         title: Text(data[index]['vrnoa']),
                  //                         subtitle: Text(data[index]['vrdate']
                  //                             .substring(0, 10)),
                  //                         trailing: IconButton(
                  //                           icon: Icon(Icons.edit),
                  //                           onPressed: () {
                  //                             vrnoaController.text =
                  //                                 data[index]['vrnoa'];
                  //                             fetchKotbyID(
                  //                                 vrnoaController.text);
                  //                             DefaultTabController.of(context)!
                  //                                 .animateTo(0);
                  //                           },
                  //                         ),
                  //                       ),
                  //                       Divider()
                  //                     ],
                  //                   );
                  //                 },
                  //               ),
                  //             ),
                  //           )
                  //         ];
                  //       } else if (snapshot.hasError) {
                  //         children = <Widget>[
                  //           const Icon(
                  //             Icons.error_outline,
                  //             color: Colors.red,
                  //             size: 60,
                  //           ),
                  //           Padding(
                  //             padding: const EdgeInsets.only(top: 16),
                  //             child: Text('Error: ${snapshot.error}'),
                  //           )
                  //         ];
                  //       } else {
                  //         children = const <Widget>[
                  //           SizedBox(
                  //             child: CircularProgressIndicator(),
                  //             width: 60,
                  //             height: 60,
                  //           ),
                  //           Padding(
                  //             padding: EdgeInsets.only(top: 16),
                  //             child: Text('Awaiting result...'),
                  //           )
                  //         ];
                  //       }

                  //       return Center(
                  //         child: Column(
                  //           mainAxisAlignment: MainAxisAlignment.center,
                  //           crossAxisAlignment: CrossAxisAlignment.center,
                  //           children: children,
                  //         ),
                  //       );
                  //     },
                  //   ),
                  // )
                ],
              ),
      ),
    );
  }

  String lastKot = '';
  addToTable(Items i,
      {bool flag = true, int qty = 1, bool old = false, String dkot = "0"}) {
    TableRows row = new TableRows(item: i, qty: qty, old: old, dKotno: dkot);

    if (flag == false) {
      row.qty = -1;
    }

    // if (row.old) {
    List<TableRows> tr = tableRows
        .where((element) => (element.item == i && element.old == row.old))
        // .where((element) => (element.item == i && element.old == true))
        .toList();
    if (tr.length < 1) {
      addRowToTable(row);
      setState(() {
        lastKot = row.dKotno;
      });
    } else {
      setState(() {
        if (lastKot != row.dKotno) {
          addRowToTable(row);
        } else {
          if (tr[tr.length - 1].qty != null)
            tr[tr.length - 1].qty =
                (tr[tr.length - 1].qty ?? 0) + (row.qty ?? 0);
        }
      });
      if ((tr[tr.length - 1].qty ?? 0) < 1) {
        tableRows.remove(tr[tr.length - 1]);
      }
    }

    calculateSum();
  }

  int sum = 0;
  addRowToTable(TableRows row) {
    setState(() {
      tableRows.add(row);
    });
  }

  sortTable(String categoryID) {
    setState(() {
      selectedItems =
          items.where((element) => element.catid == categoryID).toList();
    });
  }

  String totalCost = '';
  calculateSum() {
    setState(() {
      sum = 0;
      tableRows.forEach((element) {
        sum += int.parse(element.item!.cost ?? '0') * (element.qty ?? 0);
      });

      totalCost = sum.toString();
    });
  }

  Future<dynamic> _fetchAllOrdersList() async {
    var data = await fetchAllOrders();
    return data;
  }

  saveOrder() {
    if (tableRows.isNotEmpty) {
      showForm();
    } else {
      showSnackbar(_scaffoldKey, "Add Data in Order First", "false");
    }
  }

  getSavedObject() async {
    if (vrnoaSave == null) {
      vrnoaSave = maxVrno;
    }

    KotMain kotmain = new KotMain(
      vrnoa: vrnoaSave,
      vrdate: new DateTime.now().toString(),
      dateTime: new DateTime.now().toString(),
      etype: 'kot_bill',
      billType: billType,
      uid: currentLogin.uid,
      companyId: "1",
      customer: customerName.text,
      waiterId: waiterName.text,
      netamount: totalCost,
    );

    List<KotDetail> kotdetail = [];
    tableRows.forEach((element) {
      KotDetail detail = new KotDetail(
        itemId: element.item!.itemId,
        qty: element.qty,
        amount: ((element.qty ?? 0) * int.parse(element.item!.cost ?? '0'))
            .toString(),
        rate: element.item!.cost,
        dKotno: element.dKotno,
      );
      kotdetail.add(detail);
    });

    bool? res = await addKot(
        detail: kotdetail,
        main: kotmain,
        vrnoa: vrnoaSave ?? '',
        vtype: vouchertype);

    showSnackbar(_scaffoldKey, "Voucher Saved", res.toString());
    if (res == true) {
      FocusScopeNode currentScope = FocusScope.of(context);
      if (!currentScope.hasPrimaryFocus && currentScope.hasFocus) {
        FocusManager.instance.primaryFocus!.unfocus();
      }
      maxVrno = await getMaxVrno();
      vrnoaSave = maxVrno;
      vrnoaController.text = maxVrno;
      vouchertype = "new";
      tableRows = [];
      currentOrder = "";
      setState(() {});
    }
  }

  final customerName = new TextEditingController();
  final waiterName = new TextEditingController();
  final addressController = new TextEditingController();

  String billType = "dinein";
  showForm() {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return showDialog<AlertDialog>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          actions: <Widget>[
            OutlinedButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            MaterialButton(
              color: mainColor,
              child: Text(
                'Place',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                getSavedObject();
                Navigator.of(context).pop();
              },
            ),
          ],
          title: Text("Order Details"),
          content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return Container(
              height: height / 1.5,
              width: width / 2,
              child: DefaultTabController(
                length: 3,
                initialIndex: _currentIndex,
                child: Column(
                  children: [
                    Container(
                      child: TabBar(
                        labelColor: Colors.green,
                        unselectedLabelColor: Colors.black,
                        onTap: (index) {
                          setState(() {
                            _currentIndex = index;
                          });
                        },
                        tabs: [
                          Tab(
                              text: 'Dine In',
                              icon: Icon(Icons.fastfood_outlined)),
                          Tab(text: 'Takeaway', icon: Icon(Icons.local_dining)),
                          Tab(
                              text: 'Delivery',
                              icon: Icon(Icons.delivery_dining)),
                        ],
                      ),
                    ),
                    Container(
                      height: height / 1.5 - 75,
                      child: TabBarView(
                        children: [
                          _buildDineOrderDetailWidget(),
                          _buildTakeawayOrderDetailWidget(),
                          _buildDeliveryOrderDetailWidget()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        );
      },
    );
  }

  Widget _buildDineOrderDetailWidget() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 6.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Order# : " + (vrnoaSave ?? 'New')),
            _buildWaiterField(),
            _buildCustomerField(),
          ],
        ),
      ),
    );
  }

  Widget _buildTakeawayOrderDetailWidget() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 6.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Order# : " + (vrnoaSave ?? 'New')),
            _buildWaiterField(),
            _buildCustomerField(),
          ],
        ),
      ),
    );
  }

  Widget _buildDeliveryOrderDetailWidget() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 6.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Order# : " + (vrnoaSave ?? 'New')),
            _buildCustomerField(),
            _buildAddressField()
          ],
        ),
      ),
    );
  }

  Widget _buildCustomerField() {
    return FormBuilderTextField(
      name: "customerName",
      decoration: InputDecoration(labelText: "Customer Name"),
      controller: customerName,
      textInputAction: TextInputAction.next,
    );
  }

  Widget _buildWaiterField() {
    return FormBuilderTextField(
      name: "waiterName",
      decoration: InputDecoration(labelText: "Waiter Name"),
      controller: waiterName,
      textInputAction: TextInputAction.next,
    );
  }

  Widget _buildAddressField() {
    return FormBuilderTextField(
      name: "address",
      decoration: InputDecoration(labelText: "Address"),
      controller: addressController,
      textInputAction: TextInputAction.done,
    );
  }

  fetchKotbyID(vrno) async {
    if (vrno == null || vrno == '') {
      showSnackbar(_scaffoldKey, "Vr# field can not be empty!", "false");
      return;
    }
    FocusScopeNode currentScope = FocusScope.of(context);
    if (!currentScope.hasPrimaryFocus && currentScope.hasFocus) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
    setState(() {
      showloader = true;
    });
    tableRows = [];
    String data = await fetchKot(vrno);
    currentOrder = data;
    print(data);

    if (!(data.contains("false"))) {
      List<KotMain> main = kotMainFromMap(data);
      List<KotDetail> detail = kotDetailFromMap(data);
      detail.forEach((element) {
        // print(element.toMap());
        Items i =
            items.where((item) => item.itemId == element.itemId).toList()[0];
        setState(() {
          addToTable(i,
              qty: element.qty ?? 0, old: true, dkot: element.dKotno ?? '');
        });
      });
      setState(() {
        vrnoaSave = main[0].vrnoa!;
        vouchertype = "edit";
      });
    } else {
      setState(() {
        vrnoaSave = maxVrno;
        vouchertype = "new";
        currentOrder = "";
      });
      showSnackbar(_scaffoldKey, "No order found!", 'false');
    }
    setState(() {
      showloader = false;
    });
  }

  String? vrnoaSave;
  printList() {
    tableRows
        .sort((a, b) => int.parse(a.dKotno).compareTo(int.parse(a.dKotno)));

    tableRows.forEach((element) {
      print(element.dKotno);
    });
  }
}
