// To parse this JSON data, do
//
//     final tables = tablesFromMap(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Tables> tablesFromMap(String str) =>
    List<Tables>.from(json.decode(str).map((x) => Tables.fromMap(x)));

String tablesToMap(List<Tables> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Tables {
  Tables({
    required this.tableId,
    required this.name,
    required this.remarks,
    required this.datetime,
    required this.companyId,
    required this.active,
    required this.officerId,
    required this.tokenno,
  });

  final String tableId;
  final String name;
  final String remarks;
  final DateTime datetime;
  final String companyId;
  final String active;
  final String officerId;
  final String tokenno;

  factory Tables.fromMap(Map<String, dynamic> json) => Tables(
        tableId: json["table_id"],
        name: json["name"],
        remarks: json["remarks"],
        datetime: DateTime.parse(json["datetime"]),
        companyId: json["company_id"],
        active: json["active"],
        officerId: json["officer_id"],
        tokenno: json["tokenno"],
      );

  Map<String, dynamic> toMap() => {
        "table_id": tableId,
        "name": name,
        "remarks": remarks,
        "datetime": datetime.toIso8601String(),
        "company_id": companyId,
        "active": active,
        "officer_id": officerId,
        "tokenno": tokenno,
      };
}
