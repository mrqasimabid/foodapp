// To parse this JSON data, do
//
//     final items = itemsFromMap(jsonString);

import 'dart:convert';

List<Items> itemsFromMap(String str) =>
    List<Items>.from(json.decode(str).map((x) => Items.fromMap(x)));

String itemsToMap(List<Items> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Items {
  Items({
    this.itemId,
    this.opStock,
    this.minLevel,
    this.maxLevel,
    this.orderLevel,
    this.openDate,
    this.active,
    this.srate,
    this.srate1,
    this.srate2,
    this.costPrice,
    this.discount,
    this.status,
    this.itemCode,
    this.length,
    this.fitting,
    this.uom,
    this.itemDes,
    this.uname,
    this.description,
    this.model,
    this.photo,
    this.size,
    this.catid,
    this.itemDetailId,
    this.barcode,
    this.department,
    this.bid,
    this.madeId,
    this.subcatid,
    this.parytId,
    this.partyIdCr,
    this.netweight,
    this.grweight,
    this.uid,
    this.companyId,
    this.dateTime,
    this.shortCode,
    this.itemBarcode,
    this.departmentId,
    this.lprate,
    this.avgRate,
    this.qty,
    this.partyCode,
    this.itemDiscount,
    this.itemPurDiscount,
    this.taxexempt,
    this.taxrate,
    this.ingredPacking,
    this.ingredDesId,
    this.ingredCost,
    this.ingredUomQty,
    this.ingredUom,
    this.costOps,
    this.postingtime,
    this.color,
    this.fontcolor,
    this.inventory,
    this.cost,
    this.income,
    this.categoryName,
    this.subcategoryName,
    this.brandName,
    this.madeName,
    this.stqty,
    this.stavgRate,
    this.stweight,
  });

  String? itemId;
  String? opStock;
  String? minLevel;
  String? maxLevel;
  String? orderLevel;
  DateTime? openDate;
  String? active;
  String? srate;
  String? srate1;
  String? srate2;
  String? costPrice;
  String? discount;
  String? status;
  String? itemCode;
  String? length;
  String? fitting;
  String? uom;
  String? itemDes;
  String? uname;
  String? description;
  String? model;
  String? photo;
  String? size;
  String? catid;
  String? itemDetailId;
  String? barcode;
  String? department;
  String? bid;
  String? madeId;
  String? subcatid;
  String? parytId;
  String? partyIdCr;
  String? netweight;
  String? grweight;
  String? uid;
  String? companyId;
  dynamic dateTime;
  String? shortCode;
  String? itemBarcode;
  String? departmentId;
  String? lprate;
  String? avgRate;
  String? qty;
  String? partyCode;
  String? itemDiscount;
  String? itemPurDiscount;
  String? taxexempt;
  String? taxrate;
  String? ingredPacking;
  String? ingredDesId;
  String? ingredCost;
  String? ingredUomQty;
  String? ingredUom;
  String? costOps;
  DateTime? postingtime;
  String? color;
  String? fontcolor;
  String? inventory;
  String? cost;
  String? income;
  String? categoryName;
  String? subcategoryName;
  String? brandName;
  String? madeName;
  String? stqty;
  String? stavgRate;
  String? stweight;

  factory Items.fromMap(Map<String, dynamic> json) => Items(
        itemId: json["item_id"],
        opStock: json["op_stock"],
        minLevel: json["min_level"],
        maxLevel: json["max_level"],
        orderLevel: json["order_level"],
        openDate: DateTime.parse(json["open_date"]),
        active: json["active"],
        srate: json["srate"],
        srate1: json["srate1"],
        srate2: json["srate2"],
        costPrice: json["cost_price"],
        discount: json["discount"],
        status: json["status"],
        itemCode: json["item_code"],
        length: json["length"],
        fitting: json["fitting"],
        uom: json["uom"],
        itemDes: json["item_des"],
        uname: json["uname"],
        description: json["description"],
        model: json["model"],
        photo: json["photo"],
        size: json["size"],
        catid: json["catid"],
        itemDetailId: json["item_detail_id"],
        barcode: json["barcode"],
        department: json["department"],
        bid: json["bid"],
        madeId: json["made_id"],
        subcatid: json["subcatid"],
        parytId: json["paryt_id"],
        partyIdCr: json["party_id_cr"],
        netweight: json["netweight"],
        grweight: json["grweight"],
        uid: json["uid"],
        companyId: json["company_id"],
        dateTime: json["date_time"],
        shortCode: json["short_code"],
        itemBarcode: json["item_barcode"],
        departmentId: json["department_id"],
        lprate: json["lprate"],
        avgRate: json["avg_rate"],
        qty: json["qty"],
        partyCode: json["party_code"],
        itemDiscount: json["item_discount"],
        itemPurDiscount: json["item_pur_discount"],
        taxexempt: json["taxexempt"],
        taxrate: json["taxrate"],
        ingredPacking: json["ingred_packing"],
        ingredDesId: json["ingred_des_id"],
        ingredCost: json["ingred_cost"],
        ingredUomQty: json["ingred_uom_qty"],
        ingredUom: json["ingred_uom"],
        costOps: json["cost_ops"],
        postingtime: DateTime.parse(json["postingtime"]),
        color: json["color"],
        fontcolor: json["fontcolor"],
        inventory: json["inventory"],
        cost: json["cost"],
        income: json["income"],
        categoryName: json["category_name"],
        subcategoryName: json["subcategory_name"],
        brandName: json["brand_name"],
        madeName: json["made_name"],
        stqty: json["stqty"],
        stavgRate: json["stavg_rate"],
        stweight: json["stweight"],
      );

  Map<String, dynamic> toMap() => {
        "item_id": itemId,
        "op_stock": opStock,
        "min_level": minLevel,
        "max_level": maxLevel,
        "order_level": orderLevel,
        "open_date": openDate!.toIso8601String(),
        "active": active,
        "srate": srate,
        "srate1": srate1,
        "srate2": srate2,
        "cost_price": costPrice,
        "discount": discount,
        "status": status,
        "item_code": itemCode,
        "length": length,
        "fitting": fitting,
        "uom": uom,
        "item_des": itemDes,
        "uname": uname,
        "description": description,
        "model": model,
        "photo": photo,
        "size": size,
        "catid": catid,
        "item_detail_id": itemDetailId,
        "barcode": barcode,
        "department": department,
        "bid": bid,
        "made_id": madeId,
        "subcatid": subcatid,
        "paryt_id": parytId,
        "party_id_cr": partyIdCr,
        "netweight": netweight,
        "grweight": grweight,
        "uid": uid,
        "company_id": companyId,
        "date_time": dateTime,
        "short_code": shortCode,
        "item_barcode": itemBarcode,
        "department_id": departmentId,
        "lprate": lprate,
        "avg_rate": avgRate,
        "qty": qty,
        "party_code": partyCode,
        "item_discount": itemDiscount,
        "item_pur_discount": itemPurDiscount,
        "taxexempt": taxexempt,
        "taxrate": taxrate,
        "ingred_packing": ingredPacking,
        "ingred_des_id": ingredDesId,
        "ingred_cost": ingredCost,
        "ingred_uom_qty": ingredUomQty,
        "ingred_uom": ingredUom,
        "cost_ops": costOps,
        "postingtime": postingtime!.toIso8601String(),
        "color": color,
        "fontcolor": fontcolor,
        "inventory": inventory,
        "cost": cost,
        "income": income,
        "category_name": categoryName,
        "subcategory_name": subcategoryName,
        "brand_name": brandName,
        "made_name": madeName,
        "stqty": stqty,
        "stavg_rate": stavgRate,
        "stweight": stweight,
      };
}
