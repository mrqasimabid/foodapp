// To parse this JSON data, do
//
//     final kotSaveRes = kotSaveResFromMap(jsonString);

import 'dart:convert';

KotSaveRes kotSaveResFromMap(String str) =>
    KotSaveRes.fromMap(json.decode(str));

String kotSaveResToMap(KotSaveRes data) => json.encode(data.toMap());

class KotSaveRes {
  KotSaveRes({
    this.the0,
    this.message,
    this.pdf,
  });

  bool? the0;
  String? message;
  String? pdf;

  factory KotSaveRes.fromMap(Map<String, dynamic> json) => KotSaveRes(
        the0: json["0"] == null ? null : json["0"],
        message: json["message"] == null ? null : json["message"],
        pdf: json["pdf"] == null ? null : json["pdf"],
      );

  Map<String, dynamic> toMap() => {
        "0": the0 == null ? null : the0,
        "message": message == null ? null : message,
        "pdf": pdf == null ? null : pdf,
      };
}
