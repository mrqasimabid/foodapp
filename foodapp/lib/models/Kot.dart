// To parse this JSON data, do
//
//     final kot = kotFromMap(jsonString);

import 'dart:convert';

List<Kot> kotFromMap(String str) =>
    List<Kot>.from(json.decode(str).map((x) => Kot.fromMap(x)));

String kotToMap(List<Kot> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Kot {
  Kot({
    this.customerId,
    this.isHold,
    this.customerName,
    this.customerCell,
    this.customerAddres,
    this.vrno,
    this.waiterName,
    this.fname,
    this.rname,
    this.roomAccount,
    this.advancetax,
    this.advancetaxamount,
    this.address,
    this.bco,
    this.bqguestCid,
    this.host,
    this.bqAdvance,
    this.table,
    this.change,
    this.vrnoa,
    this.checkgst,
    this.advanceamount,
    this.cardaccount2,
    this.ratio,
    this.gst,
    this.gstamount,
    this.disc,
    this.discamount,
    this.sc,
    this.scamount,
    this.balance,
    this.cashamount,
    this.cardaccount,
    this.cardamount,
    this.netamount,
    this.mop,
    this.bookingno,
    this.orderno,
    this.pax,
    this.vrdate,
    this.dKotno,
    this.etype,
    this.rate,
    this.amount,
    this.remarks,
    this.billType,
    this.kotNo,
    this.orderType,
    this.mealType,
    this.waiterId,
    this.tableId,
    this.roomId,
    this.chId,
    this.guestCid,
    this.gid,
    this.folioId,
    this.customer,
    this.phoneNo,
    this.itemId,
    this.qty,
    this.itemName,
    this.type,
    this.kotdid,
  });

  dynamic customerId;
  String? isHold;
  dynamic customerName;
  dynamic customerCell;
  dynamic customerAddres;
  String? vrno;
  dynamic waiterName;
  dynamic fname;
  dynamic rname;
  dynamic roomAccount;
  dynamic advancetax;
  dynamic advancetaxamount;
  String? address;
  String? bco;
  dynamic bqguestCid;
  dynamic host;
  dynamic bqAdvance;
  dynamic table;
  String? change;
  String? vrnoa;
  String? checkgst;
  String? advanceamount;
  dynamic cardaccount2;
  String? ratio;
  String? gst;
  String? gstamount;
  String? disc;
  String? discamount;
  String? sc;
  String? scamount;
  String? balance;
  String? cashamount;
  String? cardaccount;
  String? cardamount;
  String? netamount;
  String? mop;
  String? bookingno;
  String? orderno;
  String? pax;
  DateTime? vrdate;
  String? dKotno;
  String? etype;
  String? rate;
  String? amount;
  String? remarks;
  String? billType;
  String? kotNo;
  String? orderType;
  String? mealType;
  String? waiterId;
  String? tableId;
  String? roomId;
  dynamic chId;
  dynamic guestCid;
  String? gid;
  dynamic folioId;
  String? customer;
  String? phoneNo;
  String? itemId;
  String? qty;
  String? itemName;
  String? type;
  String? kotdid;

  factory Kot.fromMap(Map<String, dynamic> json) => Kot(
        customerId: json["customer_id"],
        isHold: json["is_hold"],
        customerName: json["customer_name"],
        customerCell: json["customer_cell"],
        customerAddres: json["customer_addres"],
        vrno: json["vrno"],
        waiterName: json["Waiter Name"],
        fname: json["fname"],
        rname: json["rname"],
        roomAccount: json["room_account"],
        advancetax: json["advancetax"],
        advancetaxamount: json["advancetaxamount"],
        address: json["address"],
        bco: json["bco"],
        bqguestCid: json["bqguest_cid"],
        host: json["host"],
        bqAdvance: json["bq_advance"],
        table: json["Table"],
        change: json["change"],
        vrnoa: json["vrnoa"],
        checkgst: json["checkgst"],
        advanceamount: json["advanceamount"],
        cardaccount2: json["cardaccount2"],
        ratio: json["ratio"],
        gst: json["gst"],
        gstamount: json["gstamount"],
        disc: json["disc"],
        discamount: json["discamount"],
        sc: json["sc"],
        scamount: json["scamount"],
        balance: json["balance"],
        cashamount: json["cashamount"],
        cardaccount: json["cardaccount"],
        cardamount: json["cardamount"],
        netamount: json["netamount"],
        mop: json["mop"],
        bookingno: json["bookingno"],
        orderno: json["orderno"],
        pax: json["pax"],
        vrdate: DateTime.parse(json["vrdate"]),
        dKotno: json["d_kotno"],
        etype: json["etype"],
        rate: json["rate"],
        amount: json["amount"],
        remarks: json["remarks"],
        billType: json["bill_type"],
        kotNo: json["kot_no"],
        orderType: json["order_type"],
        mealType: json["meal_type"],
        waiterId: json["waiter_id"],
        tableId: json["table_id"],
        roomId: json["room_id"],
        chId: json["ch_id"],
        guestCid: json["guest_cid"],
        gid: json["gid"],
        folioId: json["folio_id"],
        customer: json["customer"],
        phoneNo: json["phone_no"],
        itemId: json["item_id"],
        qty: json["qty"],
        itemName: json["item_name"],
        type: json["type"],
        kotdid: json["kotdid"],
      );

  Map<String, dynamic> toMap() => {
        "customer_id": customerId,
        "is_hold": isHold,
        "customer_name": customerName,
        "customer_cell": customerCell,
        "customer_addres": customerAddres,
        "vrno": vrno,
        "Waiter Name": waiterName,
        "fname": fname,
        "rname": rname,
        "room_account": roomAccount,
        "advancetax": advancetax,
        "advancetaxamount": advancetaxamount,
        "address": address,
        "bco": bco,
        "bqguest_cid": bqguestCid,
        "host": host,
        "bq_advance": bqAdvance,
        "Table": table,
        "change": change,
        "vrnoa": vrnoa,
        "checkgst": checkgst,
        "advanceamount": advanceamount,
        "cardaccount2": cardaccount2,
        "ratio": ratio,
        "gst": gst,
        "gstamount": gstamount,
        "disc": disc,
        "discamount": discamount,
        "sc": sc,
        "scamount": scamount,
        "balance": balance,
        "cashamount": cashamount,
        "cardaccount": cardaccount,
        "cardamount": cardamount,
        "netamount": netamount,
        "mop": mop,
        "bookingno": bookingno,
        "orderno": orderno,
        "pax": pax,
        "vrdate": vrdate!.toIso8601String(),
        "d_kotno": dKotno,
        "etype": etype,
        "rate": rate,
        "amount": amount,
        "remarks": remarks,
        "bill_type": billType,
        "kot_no": kotNo,
        "order_type": orderType,
        "meal_type": mealType,
        "waiter_id": waiterId,
        "table_id": tableId,
        "room_id": roomId,
        "ch_id": chId,
        "guest_cid": guestCid,
        "gid": gid,
        "folio_id": folioId,
        "customer": customer,
        "phone_no": phoneNo,
        "item_id": itemId,
        "qty": qty,
        "item_name": itemName,
        "type": type,
        "kotdid": kotdid,
      };
}
