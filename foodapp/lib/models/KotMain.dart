// To parse this JSON data, do
//
//     final kotMain = kotMainFromMap(jsonString);

import 'dart:convert';

List<KotMain> kotMainFromMap(String str) =>
    List<KotMain>.from(json.decode(str).map((x) => KotMain.fromMap(x)));

String kotMainToMap(KotMain data) => json.encode(data.toMap());

class KotMain {
  KotMain({
    this.vrnoa,
    this.netamount,
    this.kotNo,
    this.etype,
    this.vrdate,
    this.dateTime,
    this.billType,
    this.waiterId,
    this.customer,
    this.uid,
    this.companyId,
    this.fnId,
    this.address,
    this.scamount = "0",
    this.gstamount = "0",
    this.discamount = "0",
    this.cardaccount = "0",
    this.mop = "0",
    this.is_hold = "1",
  });

  String? vrnoa;
  String? netamount;
  String? kotNo;
  String? etype;
  String? vrdate;
  String? dateTime;
  String? billType;
  String? waiterId;
  String? customer;
  String? uid;
  String? companyId;
  String? fnId;
  String? address;
  String? scamount;
  String? gstamount;
  String? discamount;
  String? cardaccount;
  String? mop;
  String? is_hold;

  factory KotMain.fromMap(Map<String, dynamic> json) => KotMain(
        vrnoa: json["vrnoa"],
        gstamount: json["gstamount"],
        scamount: json["scamount"],
        netamount: json["netamount"],
        kotNo: json["kot_no"],
        etype: json["etype"],
        dateTime: json["date_time"],
        vrdate: json["vrdate"],
        billType: json["order_type"],
        waiterId: json["waiter_id"],
        address: json["address"],
        customer: json["customer"],
        discamount: json["discamount"],
        cardaccount: json["cardaccount"],
        mop: json["mop"],
        is_hold: json["is_hold"],
        // uid: json["uid"],
        // companyId: json["gstamount"],
        // fnId: json["fn_id"],
      );

  Map<String, dynamic> toMap() => {
        "vrnoa": vrnoa,
        "scamount": scamount,
        "netamount": netamount,
        "kot_no": kotNo,
        "etype": etype,
        "vrdate": vrdate,
        "date_time": dateTime,
        "order_type": billType,
        "waiter_id": waiterId,
        "customer": customer,
        "uid": uid,
        "company_id": companyId,
        "discamount": discamount,
        "gstamount": gstamount,
        "cardaccount": cardaccount,
        "mop": mop,
        "is_hold": is_hold,
        "fn_id": fnId,
        "address": address ?? '',
      };
}
