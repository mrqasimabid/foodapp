import 'package:foodapp/models/Items.dart';

class TableRows {
  int? qty;
  Items? item;
  int? cost;
  bool? old;
  var dKotno;

  TableRows({
    this.cost,
    this.qty,
    this.item,
    this.old,
    this.dKotno = "0",
  });
}
