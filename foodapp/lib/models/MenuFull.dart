import 'dart:convert';

MenuFull menuFullFromMap(String str) => MenuFull.fromMap(json.decode(str));

String menuFullToMap(MenuFull data) => json.encode(data.toMap());

class MenuFull {
  MenuFull({
    this.vouchers,
    this.reports,
  });

  Map<String, Map<String, int>>? vouchers;
  Map<String, int>? reports;

  factory MenuFull.fromMap(Map<String, dynamic> json) => MenuFull(
        vouchers: Map.from(json["vouchers"]).map((k, v) =>
            MapEntry<String, Map<String, int>>(
                k, Map.from(v).map((k, v) => MapEntry<String, int>(k, v)))),
        reports: Map.from(json["reports"])
            .map((k, v) => MapEntry<String, int>(k, v)),
      );

  Map<String, dynamic> toMap() => {
        "vouchers": Map.from(vouchers!).map((k, v) => MapEntry<String, dynamic>(
            k, Map.from(v).map((k, v) => MapEntry<String, dynamic>(k, v)))),
        "reports":
            Map.from(reports!).map((k, v) => MapEntry<String, dynamic>(k, v)),
      };
}
