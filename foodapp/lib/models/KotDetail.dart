// To parse this JSON data, do
//
//     final kotDetail = kotDetailFromMap(jsonString);

import 'dart:convert';

List<KotDetail> kotDetailFromMap(String str) =>
    List<KotDetail>.from(json.decode(str).map((x) => KotDetail.fromMap(x)));

String kotDetailToMap(List<KotDetail> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class KotDetail {
  KotDetail({
    this.kotid,
    this.itemId,
    this.dKotno,
    this.rate,
    this.amount,
    this.qty,
  });

  String? kotid;
  String? itemId;
  String? dKotno;
  String? rate;
  String? amount;
  int? qty;

  factory KotDetail.fromMap(Map<String, dynamic> json) => KotDetail(
        kotid: json["kotid"] == null ? "" : json["kotid"],
        itemId: json["item_id"],
        dKotno: json["d_kotno"],
        rate: json["rate"],
        amount: json["amount"],
        qty: int.parse(json["qty"]),
      );

  Map<String, dynamic> toMap() => {
        "kotid": kotid == null ? "" : kotid,
        "item_id": itemId,
        "d_kotno": dKotno,
        "rate": rate,
        "amount": amount,
        "qty": qty,
      };
}
