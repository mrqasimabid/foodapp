// To parse this JSON data, do
//
//     final login = loginFromMap(jsonString);

import 'dart:convert';

Login loginFromMap(String str) => Login.fromMap(json.decode(str));

String loginToMap(Login data) => json.encode(data.toMap());

class Login {
  Login({
    this.companyId,
    this.companyName,
    this.contact,
    this.address,
    this.barcodePrint,
    this.footNode,
    this.uid,
    this.uname,
    this.pass,
    this.email,
    this.userType,
    this.rgid,
    this.fullname,
    this.mobile,
    this.mobCode,
    this.failedattempts,
    this.token,
    this.fnId,
    this.fstartdate,
    this.fenddate,
    this.desc,
  });

  String? companyId;
  String? companyName;
  dynamic contact;
  String? address;
  String? barcodePrint;
  String? footNode;
  String? uid;
  String? uname;
  String? pass;
  String? email;
  dynamic userType;
  String? rgid;
  String? fullname;
  String? mobile;
  String? mobCode;
  String? failedattempts;
  String? token;
  String? fnId;
  DateTime? fstartdate;
  DateTime? fenddate;
  String? desc;

  factory Login.fromMap(Map<String, dynamic> json) => Login(
        companyId: json["company_id"],
        companyName: json["company_name"],
        contact: json["contact"],
        address: json["address"],
        barcodePrint: json["barcode_print"],
        footNode: json["foot_node"],
        uid: json["uid"],
        uname: json["uname"],
        pass: json["pass"],
        email: json["email"],
        userType: json["user_type"],
        rgid: json["rgid"],
        fullname: json["fullname"],
        mobile: json["mobile"],
        mobCode: json["mob_code"],
        failedattempts: json["failedattempts"],
        token: json["token"],
        fnId: json["fn_id"],
        fstartdate: DateTime.parse(json["fstartdate"]),
        fenddate: DateTime.parse(json["fenddate"]),
        desc: json["desc"],
      );

  Map<String, dynamic> toMap() => {
        "company_id": companyId,
        "company_name": companyName,
        "contact": contact,
        "address": address,
        "barcode_print": barcodePrint,
        "foot_node": footNode,
        "uid": uid,
        "uname": uname,
        "pass": pass,
        "email": email,
        "user_type": userType,
        "rgid": rgid,
        "fullname": fullname,
        "mobile": mobile,
        "mob_code": mobCode,
        "failedattempts": failedattempts,
        "token": token,
        "fn_id": fnId,
        "fstartdate": fstartdate!.toIso8601String(),
        "fenddate": fenddate!.toIso8601String(),
        "desc": desc,
      };
}
