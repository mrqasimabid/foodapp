import 'package:flutter/material.dart';
import 'package:foodapp/pages/stackpage.dart';
import 'globels.dart';
import 'pages/homepage.dart';
import 'pages/loginPage.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    print(settings.arguments);

    // final args = settings.arguments;
    String? name = settings.name;
    // var args = (settings.arguments ?? {}) as Map;

    switch (name) {
      case '/':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/homepage':
        return MaterialPageRoute(
            builder: (_) => MyHomePage(title: "Food App", user: currentLogin));
      case '/order-dashboard':
        return MaterialPageRoute(builder: (_) => StackImplementation());
      // case '/fetchAllDoctors':
      //   return MaterialPageRoute(
      //       builder: (_) => AllDoctors(title: "All Doctors"));
      // case '/doctorProfile':
      //   return MaterialPageRoute(builder: (_) => Profile(doctor: args));
      // case 'patienthistory':
      //   return MaterialPageRoute(
      //       builder: (_) => Patienthistory(title: "Patient History"));
      // case 'patientregistration':
      //   return MaterialPageRoute(
      //       builder: (_) => PatientForm(title: 'Patient Form'));
      // case 'users':
      //   return MaterialPageRoute(builder: (_) => UserForm(title: 'User Form'));
      // case 'ipddetails':
      //   return MaterialPageRoute(
      //       builder: (_) => IDPform(title: 'IPD Registration'));
      // case 'checkin':
      //   return MaterialPageRoute(builder: (_) => CheckIn());
      // case 'profile':
      //   return MaterialPageRoute(
      //       builder: (_) => Profile(doctor: currentDoctor));
      // case 'surgery':
      //   return MaterialPageRoute(
      //       builder: (_) => SurgeryForm(title: "Surgery Form"));
      // case 'surgeryhistory':
      //   return MaterialPageRoute(
      //       builder: (_) => Surgeryhistory(title: "Surgery History"));
      // case 'lensdetails':
      //   return MaterialPageRoute(
      //       builder: (_) => Lensdetails(title: "Lens Details"));
      // case 'labtest':
      //   return MaterialPageRoute(builder: (_) => Tests(title: "Lab Tests"));
      // case 'sugarchart':
      //   return MaterialPageRoute(
      //       builder: (_) => SugarchartPage(title: "Sugar Chart"));
      // case 'chart':
      //   return MaterialPageRoute(builder: (_) => SimpleTimeSeriesChart());

      default:
        return _errorPage(settings.name!);
    }
  }

  static Route<dynamic> _errorPage(String name) {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Icon(Icons.error)),
        body: Center(
          child: Text(name.toUpperCase() + " page Coming Soon"),
        ),
        backgroundColor: Colors.red,
      );
    });
  }
}
